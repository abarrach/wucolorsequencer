#include "wuProjection.h"

wuProjection::wuProjection() {
    //    init();
}

void wuProjection::setup(int _width, int _height){

    width = _width;
    height = _height;

    fboImage.allocate(width,height, OF_IMAGE_COLOR);
    fbo.allocate(width,height);
    
    fbo.begin();
    ofClear(0,0,0,255);
    fbo.end();

    left.loadImage("right.jpg");
	right.loadImage("left.jpg");
	ofxCv::imitate(warpedColor, left);
	
	movingPoint = false;
	saveMatrix = false;
	homographyReady = false;
	
	// load the previous homography if it's available
	ofFile previous("homography.yml");
	if(previous.exists()) {
		ofxCv::FileStorage fs(ofToDataPath("homography.yml"), ofxCv::FileStorage::READ);
		fs["homography"] >> homography;
		homographyReady = true;
	}

    bCalibration = false;
    mMode = MODE_PUNTS;

}

void  wuProjection::begin(){
    fbo.begin();
}

void  wuProjection::end(){
    fbo.end();
}

void  wuProjection::calcule(){
    
    if(bCalibration)
    {
        fbo.begin();
        ofSetColor(255, 0, 0);
        ofCircle(50, 50, 10);
        ofCircle(50, height-50, 10);
        ofCircle(width-50, 50, 10);
        ofCircle(width-50, height-50, 10);
        fbo.end();
    }
    
    if(mMode== MODE_PUNTS)
    {
        ofPixels pixels;
        fbo.readToPixels(pixels);
        left.setFromPixels(pixels);
        ofxCv::imitate(warpedColor, left);
    }else
    {
        left.loadImage("right.jpg");
        ofxCv::imitate(warpedColor, left);
    }
    
    
    if(rightPoints.size() >= 4) {
		vector<ofxCv::Point2f> srcPoints, dstPoints;
		for(int i = 0; i < rightPoints.size(); i++) {
			dstPoints.push_back(ofxCv::Point2f(leftPoints[i].x - right.getWidth(), leftPoints[i].y));
			srcPoints.push_back(ofxCv::Point2f(rightPoints[i].x, rightPoints[i].y));
		}
		
		// generate a homography from the two sets of points
		homography = findHomography(ofxCv::Mat(srcPoints), ofxCv::Mat(dstPoints));
		homographyReady = true;
		
		if(saveMatrix) {
			ofxCv::FileStorage fs(ofToDataPath("homography.yml"), ofxCv::FileStorage::WRITE);
			fs << "homography" << homography;
			saveMatrix = false;
		}
	}
    
    //    warpPerspective(right, fboImage, homography, CV_INTER_LINEAR);
    //    fboImage.update();
    
	if(homographyReady) {
		// this is how you warp one ofImage into another ofImage given the homography matrix
		// CV INTER NN is 113 fps, CV_INTER_LINEAR is 93 fps
		ofxCv::warpPerspective(left, warpedColor, homography, CV_INTER_LINEAR);
		warpedColor.update();
	}

}

void  wuProjection::draw(int x, int y){
    ofSetColor(255,255);
    if(bCalibration)
    {
        if (mMode == MODE_PUNTS)
        {
            ofSetColor(255, 0, 0);
            ofCircle(50, 50, 10);
            ofCircle(50, height-50, 10);
            ofCircle(width-50, 50, 10);
            ofCircle(width-50, height-50, 10);
        }else
            left.draw(0, 0);
    }

//	left.draw(0, 0);
    //fboImage.draw(left.getWidth(), 0);
	if(homographyReady) {
//		ofEnableBlendMode(OF_BLENDMODE_MULTIPLY);
//		ofSetColor(255, 128);
		warpedColor.draw(fboImage.getWidth(), 0);
//		ofDisableBlendMode();
	}
    
//	fbo.draw(0, 0);
    
	ofSetColor(ofColor::red);
	drawPoints(rightPoints);
	ofSetColor(ofColor::blue);
	drawPoints(leftPoints);
	ofSetColor(128);
	for(int i = 0; i < leftPoints.size(); i++) {
		ofLine(rightPoints[i], leftPoints[i]);
	}

}

void wuProjection::drawPoints(vector<ofVec2f>& points) {
	ofNoFill();
	for(int i = 0; i < points.size(); i++) {
		ofCircle(points[i], 10);
		ofCircle(points[i], 1);
	}
}

bool wuProjection::movePoint(vector<ofVec2f>& points, ofVec2f point) {
	for(int i = 0; i < points.size(); i++) {
		if(points[i].distance(point) < 20) {
			movingPoint = true;
			curPoint = &points[i];
			return true;
		}
	}
	return false;
}

void wuProjection::mousePressed(int x, int y, int button) {
	ofVec2f cur(x, y);
	ofVec2f rightOffset(left.getWidth(), 0);
	if(!movePoint(leftPoints, cur) && !movePoint(rightPoints, cur)) {
		if(x > right.getWidth()) {
			cur -= rightOffset;
		}
		rightPoints.push_back(cur);
		leftPoints.push_back(cur + rightOffset);
	}
}

void wuProjection::mouseDragged(int x, int y, int button) {
	if(movingPoint) {
		curPoint->set(x, y);
	}
}

void wuProjection::mouseReleased(int x, int y, int button) {
	movingPoint = false;
}

void wuProjection::keyPressed(int key) {
    
    switch (key){
		case 's':
            saveMatrix = true;
			break;
	}
    
}

void wuProjection::calibraOnOff() {
    bCalibration=!bCalibration;
}

void wuProjection::modePunts() {
    mMode = MODE_PUNTS;
}
void wuProjection::modeEscacs() {
    mMode = MODE_ESCACS;
}


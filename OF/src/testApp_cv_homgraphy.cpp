#include "testApp.h"

using namespace ofxCv;
using namespace cv;

void testApp::setup() {
    ofSetVerticalSync(true);
    
    width = 640;
    height = 480;
    
    projec.setup(width, height);

    vidGrabber.setDeviceID(1);
    vidGrabber.setVerbose(true);
    vidGrabber.initGrabber(width,height);
    
    colorImg.allocate(width,height);

    
}

void testApp::update() {


    projec.begin();
        ofClear(0, 0, 0, 0);
        ofCircle(100,100,100);
    projec.end();

    projec.calcule();

    
    
}

void testApp::draw() {
//    ofBackground(0,0,0);
	
 	projec.draw();

}

void testApp::mousePressed(int x, int y, int button) {
    projec.mousePressed(x,y,button);
}

void testApp::mouseDragged(int x, int y, int button) {
    projec.mouseDragged(x,  y,  button);
}

void testApp::mouseReleased(int x, int y, int button) {
    projec.mouseReleased( x, y, button);
}

void testApp::keyPressed(int key) {
    projec.keyPressed(key);

    if (key=='e')
        projec.modeEscacs();
    if (key=='p')
        projec.modePunts();
    if (key=='c')
        projec.calibraOnOff();
    
}
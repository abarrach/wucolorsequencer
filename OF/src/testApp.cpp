#include "testApp.h"

std::string trim(const std::string& str,
                 const std::string& whitespace = " \t")
{
    const size_t strBegin = str.find_first_not_of(whitespace);
    if (strBegin == std::string::npos)
        return ""; // no content

    const size_t strEnd = str.find_last_not_of(whitespace);
    const size_t strRange = strEnd - strBegin + 1;

    return str.substr(strBegin, strRange);
}



bool testApp::loadSettings(const string &inFilename)
{
    bool bSuccess = false;

    ofFile file(inFilename);
    if (file.exists()) {
        stringstream ss;

        enum SettingLoaders {
            SENDER = 0,
            RECEIVER,
            PLAYER,
            CAMERA,
            MIN_AREA,
            MAX_AREA,
            HIGH_OCT,
            LOW_OCT,
            WHITE_BALANCE,
            WORKSPACE_CORNERS
        };

        int whichSetting = 0;
        for( string line; getline(file, line) && !bSuccess; ) {
            line = trim(line);
            if ( !line.empty() && line[0] != '#' ) {
                ss.str(line);
                ss.clear();
                switch (whichSetting++) {
                    case SENDER: {
                        ss >> mSenderAddr >> mSenderPort;
                    } break;
                    case RECEIVER: {
                        ss >> mReceiverPort;
                    } break;
                    case PLAYER: {
                        ss >> mPlayerNum;
                    } break;
                    case CAMERA: {
                        ss >> mCameraId;
                    } break;
                    case MIN_AREA: {
                        ss >> mMinArea;
                    } break;
                    case MAX_AREA: {
                        ss >> mMaxArea;
                    } break;
                    case HIGH_OCT: {
                        ss >> mHighOctaveThresh;
                    } break;
                    case LOW_OCT: {
                        ss >> mLowOctaveThresh;
                    } break;
                    case WHITE_BALANCE: {
                        ss >> mWBScaleRed >> mWBScaleGreen >> mWBScaleBlue;
                    } break;
                    case WORKSPACE_CORNERS: {
                        for (size_t i = 0; i < 4; i++) {
                            ss >> mWorkspaceCorners[i].x >> mWorkspaceCorners[i].y;
                        }
                        bSuccess = true;
                    } break;
                    default:
                        ofLog() << "Case " << whichSetting << " not handled!";
                        break;
                }
            }
        }
        // Now load the color database
        enum ColorLoaders {
            NAME = 0,
            DEGREE,
            HUE,
            SAT,
            BRI
        };
        int nColorLoadpoint = 0;
        ofPtr<ColorSpot> pCurColor(new ColorSpot());
        for( string line; getline(file, line); ) {
            line = trim(line);
            if ( !line.empty() && line[0] != '#' ) {
                double mean, var;
                ss.str(line);
                ss.clear();
                switch (nColorLoadpoint++) {
                    case NAME:
                        ss >> pCurColor->mName;
                        break;
                    case DEGREE:
                        ss >> pCurColor->mDegree;
                        break;
                    case HUE:
                        ss >> mean >> var;
                        pCurColor->mHue.setMeanAndVariance(mean, var);
                        break;
                    case SAT:
                        ss >> mean >> var;
                        pCurColor->mSat.setMeanAndVariance(mean, var);
                        break;
                    case BRI:
                        ss >> mean >> var;
                        pCurColor->mBri.setMeanAndVariance(mean, var);
                        ofLog() << "Loading a color: " << pCurColor->mName << " Degree: " << pCurColor->mDegree;
                        pCurColor->mClassifier.addFeature(ofPtr<GaussStatBase>(new GaussCircularStat(pCurColor->mHue.getMean(), pCurColor->mHue.getVariance())));
                        pCurColor->mClassifier.addFeature(ofPtr<GaussStatBase>(new GaussLinearStat(pCurColor->mSat.getMean(), pCurColor->mSat.getVariance())));
#ifdef USE_BRIGHTNESS_FEATURE
                        pCurColor->mClassifier.addFeature(ofPtr<GaussStatBase>(new GaussLinearStat(pCurColor->mBri.getMean(), pCurColor->mBri.getVariance())));
#endif

                        pCurColor->mClassifier.setProb(0.5);  // Doesn't really matter what we put here for now,
                                                            // as long as they're all the same.
                        mColorData.push_back(*pCurColor);
                        pCurColor = ofPtr<ColorSpot>(new ColorSpot());
                        nColorLoadpoint = 0;  // Now start another color
                        break;
                    default:
                        ofLogWarning() << "Shouldn't have gotten here!";
                        break;
                }
            }
        }
#ifdef USE_BRIGHTNESS_FEATURE
        ofLog() << "Including Brightness as a feature";
#else
        ofLog() << "Not including Brightness as a feature";
#endif

    }
    else {
        ofLogError() << "File " << inFilename << " not found.";
    }

    return bSuccess;
}

void testApp::saveSettings(const string &inFilename)
{
    ofFile file(inFilename, ofFile::WriteOnly);
    file << "# sender_addr server_port" << endl;
    file << mSenderAddr << " " << mSenderPort << endl;
    file << endl;
    file << "# receive_port" << endl;
    file << mReceiverPort << endl;
    file << endl;
    file << "# player number (1-6)" << endl;
    file << mPlayerNum << endl;
    file << endl;
    file << "# camera device number (0-N) -- index of the webcam to use, normally 0 or 1" << endl;
    file << mCameraId << endl;
    file << endl;
    file << "# minimum blob area in pixels" << endl;
    file << mMinArea << endl;
    file << endl;
    file << "# maximum blob area in pixels" << endl;
    file << mMaxArea << endl;
    file << endl;
    file << "# below this area (in pixels), we shift up an octave" << endl;
    file << mHighOctaveThresh << endl;
    file << endl;
    file << "# above this area (in pixels), we shift down an octave" << endl;
    file << mLowOctaveThresh << endl;
    file << endl;
    file << "# white balance color scaling (Red Green Blue)" << endl;
    file << mWBScaleRed << " " << mWBScaleGreen << " " << mWBScaleBlue << endl;
    file << endl;
    file << "# workspace corners (4 XY coords --> top left, right, bottom right, left -- i.e. clockwise from upper left)" << endl;
    for (size_t i = 0; i < 4; i++) {
        file << "\t\t" << mWorkspaceCorners[i].x << " " << mWorkspaceCorners[i].y;
    }
    file << endl << endl;

    file << "# Stats for colors, in the following format:" << endl;
    file << "#   colorName" << endl;
    file << "#   noteDegree (in scale: 0 = root, 7 = up one octave)" << endl;
    file << "#   hueMean hueVariance (hue is -180 to 180 or 0-360 degrees)" << endl;
    file << "#   saturationMean saturationVariance (sat is 0-255)" << endl;
    file << "#   brightnessMean brightnessVariance (bri is 0-255)" << endl;
    file << endl;

    for (size_t i = 0; i < mColorData.size(); i++) {
        ColorSpot &curColor = mColorData[i];
        file << curColor.mName << endl;
        file << curColor.mDegree << endl;
        file << round(curColor.mHue.getMean()) << " " << curColor.mHue.getVariance() << endl;
        file << round(curColor.mSat.getMean()) << " " << round(curColor.mSat.getVariance()) << endl;
        file << round(curColor.mBri.getMean()) << " " << round(curColor.mBri.getVariance()) << endl;
        file << endl;
    }
    
    ofLog() << "Saved settings file: '" << inFilename << "'";
}

//--------------------------------------------------------------
void testApp::setup()
{
    mViewportWidth = 640;
    mViewportHeight = 480;
    mCurStep = 0;
    mCurBar = 0;
    mSenderAddr = "192.168.1.19";   // Needs to be set to the server's address (using config file)
    mSenderPort = 13580;            // Default SuperCollider language port
    mReceiverPort = 13579;          // Port we receive on; needs to match the SC program
    mPlayerNum = 1;                 // Which player are we (1-6)?
    mCameraId = 1;                  // Which camera index to use (normally 1, or 0 if you have no built-in webcam)
    mMinArea = 10 * 10;             // we ignore blobs below this size in pixels
    mMaxArea = 50 * 50;             // we ignore blobs above this size in pixels
    mHighOctaveThresh = 400;        // below this area (in pixels), we shift up an octave
    mLowOctaveThresh = 1200;        // above this area (in pixels), we shift down an octave
    mShowHistograms = false;
    mShowHelp = false;
    mOnline = false;
    
	mCaptureWidth = 640;
	mCaptureHeight = 480;

    mWorkspaceCorners[0].set(0, 0);
    mWorkspaceCorners[1].set(mCaptureWidth, 0);
    mWorkspaceCorners[2].set(mCaptureWidth, mCaptureHeight);
    mWorkspaceCorners[3].set(0, mCaptureHeight);
    mSettingCornerId = -1;
    
    mWBScaleRed = mWBScaleGreen = mWBScaleBlue = 1.0;

    mSteps.resize(kNumSteps);

    // Load configuration file
    if (!loadSettings("sixPianosSetup.txt")) {
        ofLogWarning() << "Problem with setup file - OSC may not be correctly configured.";
    }

 	ofBackground(0, 0, 0);
	ofSetFrameRate(20);
	ofSetVerticalSync(false);
	mDrawBlobs = true;
	mMaInvisible = false;
	mThreshold		 = 24;
    mStatus = STAT_NONE;

    mInput.listDevices();

#ifdef USE_UVC_CAMERA
    string cameraName = "Logitech Camera";
    int vendorId = 0x046d;
    int productId = 0x0826;
    int interfaceNum = 2;

    mInput.setDeviceID(mCameraId);
    mInput.initGrabber(mCaptureWidth, mCaptureHeight);

//    focus = 0.5;

    mUvcControl.useCamera(vendorId, productId, interfaceNum);
    mUvcControl.setAutoExposure(false);
    mUvcControl.setWhiteBalance(0/10000.0);
    mUvcControl.setAutoWhiteBalance(false);
    vector<ofxUVCControl> controls = mUvcControl.getCameraControls();
    for (int i = 0; i < controls.size(); i++) {
        ofLog() << i << ": " << controls[i].name << " -- " << controls[i].supportsGet();
    }
#else
    mInput.setDeviceID(mCameraId);
	mInput.initGrabber(mCaptureWidth, mCaptureHeight);
	mInput.setDesiredFrameRate(60);
#endif

	mColor.allocate(mCaptureWidth, mCaptureHeight);
	mColorPlus.allocate(mCaptureWidth, mCaptureHeight, OF_IMAGE_COLOR);
	mGray.allocate(mCaptureWidth, mCaptureHeight);
	mGrayBg.allocate(mCaptureWidth, mCaptureHeight);
	mGrayDiff.allocate(mCaptureWidth, mCaptureHeight);
	mGrayPlus.allocate(mCaptureWidth, mCaptureHeight);
//	contourAnalysis.setSize(mCaptureWidth, mCaptureHeight);

    // Setup OSC communications
    mSender.setup(mSenderAddr, mSenderPort);
    ofLog() << "Using server: '" << mSenderAddr << "' on port " << mSenderPort;
    mReceiver.setup(mReceiverPort);
    ofLog() << "Receiving on port: " << mReceiverPort;
    ofLog() << "We will be player #" << mPlayerNum;

    // Other OF setup
    mFont.loadFont("Arial.ttf", 14);
    mFontSmall.loadFont("Arial.ttf", 11);

    for(int i=0; i<kNumLevels; i++) {
        mFracHue[i]=0;
        mFracSat[i]=0;
        mFracBri[i]=0;
    }

    // HOMOGRAPHY
    mProjector.setup(mViewportWidth, mViewportHeight);
    mProjectorToggle = false;
}


//--------------------------------------------------------------
void testApp::update()
{
	mInput.update();

	if(mInput.isFrameNew()) {

		mColor.setFromPixels(mInput.getPixels(), mCaptureWidth, mCaptureHeight);
        if (mSettingCornerId < 0) {
            // We're not in setting corners mode, so distort to match the rectangle!
            mColor.warpPerspective(mWorkspaceCorners[0], mWorkspaceCorners[1], mWorkspaceCorners[2], mWorkspaceCorners[3]);
        }
		if(mMaInvisible)
		{
		    // si vull eliminar el BLANC
			mGrayPlus.deleteWhite(&mColor);
			mGray.setFromPixels(mGrayPlus.getPixels(), mCaptureWidth, mCaptureHeight);
		}else
            mGray = mColor;

		if (mLearnBackground == true){
			mGrayBg = mGray;		// the = sign copys the pixels from grayImage into grayBg (operator overloading)
			mLearnBackground = false;
			mStatus = STAT_BG;
		}

		// take the abs value of the difference between background and incoming and then threshold:
		mGrayDiff.absDiff(mGrayBg, mGray);
		mGrayDiff.threshold(mThreshold);

		mGray.threshold(mThreshold, false);

		mContour.findContours(mGrayDiff, 100, (mCaptureWidth*mCaptureHeight)/3, MAX_BLOBS, true);

        bool bNewMsg = listenOsc();
        if (mCurStep > 0) {

            evaluateBlobsAtStep(mCurStep);

            if (bNewMsg) {
                sendNotesAtStep(mCurStep - 1);
            }
        }
	}

    // HOMOGRAPHY
    mProjector.begin();
    ofBackground(0,0,0);
    drawHighlight();
    mProjector.end();
    mProjector.calcule();    
}

void testApp::evaluateBlobsAtStep(int inStep)
{
    mBlobInfo.clear();
    TimeStep &step = mSteps[inStep - 1];
    step.mNotes.clear();

    for(int j=0; j<mContour.blobs.size();j++) {

        BlobInfoSt blobInfoTmp;
        isActive_blob_info( &(mContour.blobs[j]), inStep, &(blobInfoTmp));

        if(blobInfoTmp.isActive)
        {
            blobInfoTmp.color = calc_color(&(mContour.blobs[j]), mColor.getPixels(), mGray.getPixels());

            get_blob_info( &(mContour.blobs[j]), &(blobInfoTmp));

            mBlobInfo.push_back(blobInfoTmp);

            // ENVIO INFO A SC via OSC
            Note note;
            note.degree = blobInfoTmp.type;

            // octava definida per la Y i no pel tamany
            if (blobInfoTmp.posY < 0.333)
                note.octave = 1;
            else if (blobInfoTmp.posY > 0.666)
                note.octave = -1;
            else
                note.octave = 0;

            /*
            if (blobInfoTmp.area < mHighOctaveThresh)
                note.octave = 1;
            else if (blobInfoTmp.area > mLowOctaveThresh)
                note.octave = -1;
            else
                note.octave = 0;
*/
            note.volume = (int) ((blobInfoTmp.area * 127. )/ mMaxArea);
            note.pos.x = 0.;
            note.pos.y = blobInfoTmp.posY;
            step.mNotes.push_back(note);
        }
    }
}

//--------------------------------------------------------------
void testApp::draw()
{

	string info;
	ofSetHexColor(0xffffff);

    int viewportWidth = mViewportWidth; //ofGetViewportWidth();
    int viewportHeight = mViewportHeight; //ofGetViewportHeight();
    
    if(mDrawBlobs)
	{
		mGrayDiff.draw(ofRectangle(0, 0, viewportWidth, viewportHeight));

        ofPushMatrix();
        ofScale(viewportWidth / (float)mCaptureWidth, viewportHeight / (float)mCaptureHeight);
        
		for (size_t i = 0; i < mBlobInfo.size(); i++){

			ofSetColor(255,255,255);
			if(mBlobInfo[i].isActive)
			{
                int octave = 0;
                if (mBlobInfo[i].area > mLowOctaveThresh)
                    octave = -1;
                else if (mBlobInfo[i].area < mHighOctaveThresh)
                    octave = 1;

				info = mColorData[mBlobInfo[i].type].mName + " (" + ofToString(octave) + ")";
				ofDrawBitmapString(info, mBlobInfo[i].min_pt.x, mBlobInfo[i].min_pt.y-10);

				info = "h:"+ofToString((float)mBlobInfo[i].color.getHue(), 0);
				ofDrawBitmapString(info, mBlobInfo[i].min_pt.x, mBlobInfo[i].min_pt.y-20);

				info = "l:"+ofToString((float)mBlobInfo[i].color.getBrightness(), 0);
				ofDrawBitmapString(info, mBlobInfo[i].min_pt.x, mBlobInfo[i].min_pt.y-30);

				info = "a:"+ofToString((float)mBlobInfo[i].area, 0);
				ofDrawBitmapString(info, mBlobInfo[i].min_pt.x, mBlobInfo[i].min_pt.y-40);


                ofFill();
				ofSetColor(mBlobInfo[i].color);
				ofRect(mBlobInfo[i].min_pt.x, mBlobInfo[i].min_pt.y-60, 70, 10);
				ofNoFill();
			}
		}
        ofFill();
        for (int i = 0; i < mContour.blobs.size(); i++)
            mContour.blobs[i].draw(0,0);

        ofPopMatrix();
        
        if (mShowHistograms) {
            ofFill();
            int maxHeight = viewportHeight / 2;
            ofColor col;
            int binWidth = viewportWidth / 3 / kNumLevels;

            for(int i=0; i<kNumLevels; i++) {
                col.setHsb(i * kLevelSkip, 196, 128);
                ofSetColor(col);
                ofRect(0 + i * binWidth, viewportHeight - mFracHue[i] * maxHeight,
                       binWidth, mFracHue[i] * maxHeight);

                col.setHsb(32, i * kLevelSkip, 160);
                ofSetColor(col);
                ofRect(viewportWidth / 3 + i * binWidth, viewportHeight - mFracSat[i] * maxHeight,
                       binWidth, mFracSat[i] * maxHeight);

                col.setHsb(0, 0, i * kLevelSkip);
                ofSetColor(col);
                ofRect(2 * viewportWidth / 3 + i * binWidth, viewportHeight - mFracBri[i] * maxHeight,
                       binWidth, mFracBri[i] * maxHeight);
            }
        }


	}else{
	    // muestra la camara
		mColor.draw(ofRectangle(0, 0, viewportWidth, viewportHeight));

        ofPushMatrix();
        ofScale(viewportWidth / (float)mCaptureWidth, viewportHeight / (float)mCaptureHeight);

        ofFill();
        for (int i = 0; i < mContour.blobs.size(); i++)
            mContour.blobs[i].draw(0,0);

        drawHighlight();
        
        ofPopMatrix();
	}

    if(mStatus == STAT_NONE)
    {
        ofSetColor(255, 0, 0);
        info = "press SPACE";
        ofDrawBitmapString(info, 200, 240);
    }


    ofEnableAlphaBlending();
    for(int i=1; i < kNumSteps+1; i++)
    {
        int step_max = i * viewportWidth/kNumSteps;
        int step_min = (i * viewportWidth/kNumSteps) - viewportWidth/kNumSteps;
        ofSetColor(255,255,255);
        info = ofToString(i);
        ofDrawBitmapString(info, (step_max + step_min)/2, 10);

        ofSetColor(192,192,192,32);
        ofLine(step_max, 0, step_max, viewportHeight);
    }

    if(mCurStep>0)
    {
        ofSetColor(255, 255, 0, 50);
		ofFill();
        ofRect((mCurStep-1)* (viewportWidth/kNumSteps), 0, (viewportWidth/kNumSteps), 10);
    }
    ofDisableAlphaBlending();

    if (mSettingCornerId < 0) {
        // We are not setting corners...
        
        if (mShowHistograms) {
            //
            // Draw the palette of colours, for calibration
            //
            int colorRadius = min(viewportWidth / 42, viewportHeight / 32);
            int x = viewportWidth - colorRadius * 2;
            int y = colorRadius * 2;
            
            ofFill();
            for (size_t i = 0; i < mColorData.size(); i++) {
                y = colorRadius * 2 + ((mColorData.size() - i)) * colorRadius * 2;
                ofColor col;
                float hue = mColorData[i].mHue.getMean();
                if (hue < 0)
                    hue += 360;
                hue *= 255.0 / 360.0;
                col.setHsb(hue, mColorData[i].mSat.getMean(), mColorData[i].mBri.getMean());
                ofSetColor(col);
                ofCircle(x, y, colorRadius);
                ofSetColor(255,255,255);
                mFontSmall.drawString(mColorData[i].mName, x - colorRadius - 60, y);
            }
            
            string str = "Click to:  (set WB)\n         (reset WB)";
            int posX = viewportWidth - mFontSmall.stringWidth(str) - 5;
            int posY = viewportHeight - 5 - 6 * mFontSmall.getLineHeight();
            mWhiteBalanceRect = mFontSmall.getStringBoundingBox(str, posX, posY);
            ofSetColor(60, 60, 60);
            mFontSmall.drawString(str, posX + 2, posY + 2);
            ofSetColor(240, 240, 240);
            mFontSmall.drawString(str, posX, posY);
            
            str = "Click to: (set corners)";
            posY = viewportHeight - 5 - 8 * mFontSmall.getLineHeight();
            mResetCornersRect = mFontSmall.getStringBoundingBox(str, posX, posY);
            ofSetColor(60, 60, 60);
            mFontSmall.drawString(str, posX + 2, posY + 2);
            ofSetColor(240, 240, 240);
            mFontSmall.drawString(str, posX, posY);
        }
    }
    else {
        // We are setting corners, give some instructions:
        int posX = viewportWidth / 5;
        int posY = viewportHeight / 4;
        stringstream ss;
        ss << "Capturing corners -- clockwise from top left:\nClick inside workspace corners\n\n         ";
        switch (mSettingCornerId) {
            case 0:
                ss << "TOP LEFT";
                break;
            case 1:
                ss << "TOP RIGHT";
                break;
            case 2:
                ss << "BOTTOM RIGHT";
                break;
            case 3:
                ss << "BOTTOM LEFT";
                break;
        }
        ofSetColor(80, 30, 50);
        mFont.drawString(ss.str(), posX+2, posY+2);
        ofSetColor(240, 100, 150);
        mFont.drawString(ss.str(), posX, posY);
    }
    
    ofSetColor(200, 200, 200);
    mFont.drawString(mOnline ? "(o)nline" : "(o)ffline", 5, viewportHeight - 5);
    stringstream ss;
    int posX = viewportWidth / 4;
    int posY = viewportHeight - 5;
    ss << "Current bar: " << mCurBar << "\n\nPlayer #" << mPlayerNum;
    mFontSmall.drawString(ss.str(), posX, posY - 2 * mFontSmall.getLineHeight());

    if (mShowHelp) {
        ss.str("");
        ss.clear();
        ss << "'h' toggles histogram\n'?' toggles help\n<backspace> sends notes-off\n";
        ss << "Server: '" << mSenderAddr << "'\nSend to: " << mSenderPort << ", recv on: " << mReceiverPort;

        posX = 3 * viewportWidth / 5;
        mFontSmall.drawString(ss.str(), posX, posY - 4 * mFontSmall.getLineHeight());
    }

    // HOMOGRAPHY
    mProjector.draw();
}

string testApp::logStats(const StatsBase &inStats) const
{
    stringstream ss;
    ss << ofToString(inStats.getMean(), 1) << " " << ofToString(inStats.getVariance(), 3);
    return ss.str();
}

//--------------------------------------------------------------
void testApp::keyPressed(int key)
{


	// gray threshold
	if(key == OF_KEY_UP) {
		mThreshold ++;
		if(mThreshold > 255) mThreshold = 255;
        cout << mThreshold << " ";
	}
	if(key == OF_KEY_DOWN) {
		mThreshold --;
		if(mThreshold < 0) mThreshold = 0;
       cout << mThreshold << " ";
	}

    if(key == OF_KEY_BACKSPACE)
        sendNotesOff();

	if(key == ' ')
        mLearnBackground	= true;

	if(key == '1') mCurStep = 1;
	if(key == '2') mCurStep = 2;
	if(key == '3') mCurStep = 3;
	if(key == '4') mCurStep = 4;
	if(key == '5') mCurStep = 5;
	if(key == '6') mCurStep = 6;
	if(key == '7') mCurStep = 7;
	if(key == '8') mCurStep = 8;

    if(key == 'c') {
        mWorkspaceCorners[0].set(0, 0);
        mWorkspaceCorners[1].set(mCaptureWidth, 0);
        mWorkspaceCorners[2].set(mCaptureWidth, mCaptureHeight);
        mWorkspaceCorners[3].set(0, mCaptureHeight);
        mSettingCornerId = -1;
        saveSettings("sixPianosSetup.txt");
    }
	if(key == 'v') mDrawBlobs = !mDrawBlobs;
	if(key == 'm') mMaInvisible = !mMaInvisible;

	if(key == 's') 	mInput.videoSettings();

//	if(key == 'd') 	osc_param(3001, "gate", 0);

    if (key == 'o') {
        mOnline = !mOnline;
        if (mOnline) {
            // We just came online, so send all the current notes
            for (int i = 0; i < kNumSteps; i++) {
                evaluateBlobsAtStep(i + 1);     // expects 1-8
                sendNotesAtStep(i);             // expects 0-7
            }
        }
    }

    if(key == '?') mShowHelp = !mShowHelp;

    if(key == 'i') {
        // Dump the color info stats for the current blob
        ofLog() << "Current blob stats (mean and variance for R/G/B)" << endl <<
        logStats(mStatsHue) << endl << logStats(mStatsSat) << endl << logStats(mStatsBri);
    }

    if(key == 'h')
        mShowHistograms = !mShowHistograms;

#ifdef USE_UVC_CAMERA
    if(key == 'W') {
        mWhiteBalance = mUvcControl.getWhiteBalance();
        ofLog() << "White Balance: " << mWhiteBalance;
    }
    if(key == 'w') mUvcControl.setAutoWhiteBalance(!mUvcControl.getAutoWhiteBalance());
    if(key == 'e') mUvcControl.setAutoExposure(!mUvcControl.getAutoExposure());
#endif

    // HOMOGRAPHY
    mProjector.keyPressed(key);
    if (key == 'T') mProjectorToggle = !mProjectorToggle;
    if (key == 'E') mProjector.modeEscacs();
    if (key == 'P') mProjector.modePunts();
    if (key == 'C') mProjector.calibraOnOff();
}

//--------------------------------------------------------------
void testApp::mouseMoved(int mouseX, int mouseY )
{
}

//--------------------------------------------------------------
void testApp::mouseDragged(int mouseX, int mouseY, int button)
{
    if (mProjectorToggle)
        mProjector.mouseDragged(mouseX, mouseY, button);
}

//--------------------------------------------------------------
void testApp::mousePressed(int mouseX, int mouseY, int button)
{
    if (mSettingCornerId >= 0) {
        int posX = mouseX * mCaptureWidth / mViewportWidth; //ofGetViewportWidth();
        int posY = mouseY * mCaptureHeight / mViewportHeight; //ofGetViewportHeight();
        mWorkspaceCorners[mSettingCornerId].set(posX, posY);
        mSettingCornerId++;
        if (mSettingCornerId > 3) {
            // We're done setting!  Save to file and get out of setting mode.
            saveSettings("sixPianosSetup.txt");
            mSettingCornerId = -1;
        }
    }
    else if (mShowHistograms) {
        if (mWhiteBalanceRect.inside(mouseX, mouseY)) {
            ofRectangle setRect(mWhiteBalanceRect);
            setRect.height /= 2;
            if (setRect.inside(mouseX, mouseY)) {
                setWhiteBalance();
            }
            else {
                // Reset white balance
                ofLog() << "Reset white balance.";
                mWBScaleRed = mWBScaleGreen = mWBScaleBlue = 1.0;
            }
            saveSettings("sixPianosSetup.txt");
        }
        if (mResetCornersRect.inside(mouseX, mouseY)) {
            // Turn on "set corners" mode
            mSettingCornerId = 0;
            return;
        }

        int colorRadius = min(mViewportWidth / 42, mViewportHeight / 32);
        int x = mViewportWidth - colorRadius * 2;
        int y;

        int dividingX = x - colorRadius;
        bool bAverage = (button != 0);
        float mixAmount = bAverage ? 0.5 : 0;
        if (mixAmount > 0) {
            ofLog() << "Mixing in color with fraction: " << mixAmount;
        }

        if (mouseX >= dividingX) {
            //
            // See if we clicked on the note "palette" to choose a new colour
            //
            for (size_t i = 0; i < mColorData.size(); i++) {
                y = colorRadius * 2 + ((mColorData.size() - i)) * colorRadius * 2;
                float dist = sqrt(pow(mouseX - x, 2.0) + pow(mouseY - y, 2.0));
                if (dist < colorRadius) {
                    ColorSpot &curColor = mColorData[i];
                    curColor.mHue.setMeanAndVariance(
                                                     (1 - mixAmount) * mStatsHue.getMean() + mixAmount * curColor.mHue.getMean(),
                                                     (1 - mixAmount) * mStatsHue.getVariance() + mixAmount * curColor.mHue.getVariance());
                    ofLog() << "Set hue to: " << curColor.mHue.getMean();
                    curColor.mSat.setMeanAndVariance(
                                                     (1 - mixAmount) * mStatsSat.getMean() + mixAmount * curColor.mSat.getMean(),
                                                     (1 - mixAmount) * mStatsSat.getVariance() + mixAmount * curColor.mSat.getVariance());
                    curColor.mBri.setMeanAndVariance(
                                                     (1 - mixAmount) * mStatsBri.getMean() + mixAmount * curColor.mBri.getMean(),
                                                     (1 - mixAmount) * mStatsBri.getVariance() + mixAmount * curColor.mBri.getVariance());

                    curColor.mClassifier.reset();
                    curColor.mClassifier.addFeature(ofPtr<GaussStatBase>(new GaussCircularStat(curColor.mHue.getMean(), curColor.mHue.getVariance())));
                    curColor.mClassifier.addFeature(ofPtr<GaussStatBase>(new GaussLinearStat(curColor.mSat.getMean(), curColor.mSat.getVariance())));
#ifdef USE_BRIGHTNESS_FEATURE
                    curColor.mClassifier.addFeature(ofPtr<GaussStatBase>(new GaussLinearStat(curColor.mBri.getMean(), curColor.mBri.getVariance())));
#endif

                    curColor.mClassifier.setProb(0.5);  // Doesn't really matter what we put here for now,
                    // as long as they're all the same.

                    saveSettings("sixPianosSetup.txt");

                    break;
                }
            }
        }
    }

    if (mProjectorToggle)
        mProjector.mousePressed(mouseX, mouseY, button);
}

//--------------------------------------------------------------
void testApp::mouseReleased(int mouseX, int mouseY, int button)
{
    if (mProjectorToggle)
        mProjector.mouseReleased(mouseX, mouseY, button);
}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h)
{
}

void testApp::setWhiteBalance()
{
    ofColor col;
    float hue = mStatsHue.getMean();
    if (hue < 0)
        hue += 360;
    hue *= 255.0 / 360.0;
    col.setHsb(hue, mStatsSat.getMean(), mStatsBri.getMean());
    float ref = min(min(col.r, col.g), col.b);
    
    mWBScaleRed = ref / col.r;
    mWBScaleGreen = ref / col.g;
    mWBScaleBlue = ref / col.b;
    ofLog() << "Set WB: " << mWBScaleRed << ", " << mWBScaleGreen << ", " << mWBScaleBlue;
}

ofColor testApp::calc_color(const ofxCvBlob * blob, unsigned char * colorPixels, unsigned char * maskPixels)
{

    ofColor mixColor;
    float hue, sat, bri;
    int counterHue[kNumLevels], counterSat[kNumLevels], counterBri[kNumLevels];
	int maxHue=0;
	int maxSat=0;
	int maxBri=0;

    for(int i=0; i<kNumLevels; i++) {
        counterHue[i] = 0;
        counterSat[i]=0;
        counterBri[i]=0;
    }

    int numPixels = 0;
    double rgbSums[3] = { 0, 0, 0 };

    mStatsHue.start();
    mStatsSat.start();
    mStatsBri.start();

    // compose final result
	for(int i=blob->boundingRect.x; i < blob->boundingRect.x + blob->boundingRect.width; i++) {
//    for(int i=0; i<40; i++){
		for(int j=blob->boundingRect.y; j<blob->boundingRect.y + blob->boundingRect.height; j++){
           int mainPixelPos = (j*mCaptureWidth + i) * 3;		//pixel position of video
           int maskPixelPos = (j*mCaptureWidth + i);		//pixel position of mask

           ofColor initColor(colorPixels[mainPixelPos] * mWBScaleRed, colorPixels[mainPixelPos+1] * mWBScaleGreen, colorPixels[mainPixelPos+2] * mWBScaleBlue);
		   if (maskPixels[maskPixelPos]>32) // només els pixels del blob
		   {
               if (initColor.r + initColor.g + initColor.b < (248 * 3)) {
                   initColor.getHsb(hue, sat, bri);
                   mStatsHue.addDataPoint(initColor.getHue() * 360.0 / 255.0);  // convert from 0-255 to degrees
                   mStatsSat.addDataPoint(initColor.getSaturation());
                   mStatsBri.addDataPoint(initColor.getBrightness());
                   numPixels++;
                   rgbSums[0] += initColor.r;
                   rgbSums[1] += initColor.g;
                   rgbSums[2] += initColor.b;
                   counterHue[((int)hue) / kLevelSkip]++;
                   counterSat[((int)sat) / kLevelSkip]++;
                   counterBri[((int)bri) / kLevelSkip]++;
               }
		   }
       }
    }

    // en realidad no és la media sino el color de pixel mas repetido
	for(int i=0; i < kNumLevels;i++)
	{
		if(counterHue[i] > maxHue)
		{
		    maxHue=counterHue[i];
			hue = (i + 0.5) * kLevelSkip;
		}
		if(counterSat[i] > maxSat)
		{
		    maxSat=counterSat[i];
			sat = (i + 0.5) * kLevelSkip;
		}
		if(counterBri[i] > maxBri)
		{
		    maxBri=counterBri[i];
			bri = (i + 0.5) * kLevelSkip;
		}
	}

//    ofLog() << "H " << hue << " S " << sat << " B " << bri;
    if (numPixels == 0) {
        // Prevent divide by zero
        //ofLog() << "Num pixels == 0!";
        numPixels = 1;
    }
    mixColor.setHsb(hue, sat, bri);
    mixColor.set(0.5 * mixColor.r + 0.5 * rgbSums[0] / numPixels,
                 0.5 * mixColor.g + 0.5 * rgbSums[1] / numPixels,
                 0.5 * mixColor.b + 0.5 * rgbSums[2] / numPixels);
//    mixColor.set(rgbSums[0] / numPixels,
//                 rgbSums[1] / numPixels,
//                 rgbSums[2] / numPixels);

    // Copy the blob's histogram for display, easing it into new values to smooth it out
    for(int i=0; i<kNumLevels; i++) {
        mFracHue[i] = 0.5 * mFracHue[i] + 0.5 * counterHue[i] / (double) numPixels;
        mFracSat[i] = 0.5 * mFracSat[i] + 0.5 * counterSat[i] / (double) numPixels;
        mFracBri[i] = 0.5 * mFracBri[i] + 0.5 * counterBri[i] / (double) numPixels;
    }

	return mixColor;
}

void testApp::isActive_blob_info(const ofxCvBlob * blob, int inStep, BlobInfoSt * info)
{
    info->min_pt.y = 2000;
    info->max_pt.y = 0;
    info->isActive = false;
    int step_max = inStep * mCaptureWidth/kNumSteps ;
    int step_min = (inStep * mCaptureWidth/kNumSteps) - mCaptureWidth/kNumSteps;

    info->min_pt.x = blob->boundingRect.x;
    info->min_pt.y = blob->boundingRect.y;

    info->max_pt.x = blob->boundingRect.x + blob->boundingRect.width;
    info->max_pt.y = blob->boundingRect.y + blob->boundingRect.height;


    if( ((info->min_pt.x > step_min) && (info->min_pt.x < step_max)) )
    {
        if(blob->area > mMinArea && blob->area < mMaxArea)
            info->isActive = true;
    }
}

void testApp::get_blob_info(const ofxCvBlob * blob, BlobInfoSt * info)
{
    // altres valors
    //				info->tightness = blob->boundingRect.width/blob->boundingRect.height;
    info->area = blob->area;
    //				info->area_per = (blob->boundingRect.width*blob->boundingRect.height)/blob->area;
    //				info->length = blob->length;
    //				info->area_tight = info->area_per * info->tightness * 1000/info->area;
    //				info->gruix = info->max_pt.y-info->min_pt.y;
    info->posY = blob->boundingRect.y / mCaptureHeight;

    // Here is where we decide which color label to give the blob
    double maxFeatDens = 0;
    int maxFeatIndex = 0;
    FeatureVec features;
#ifdef USE_BRIGHTNESS_FEATURE
    features.resize(3);
#else
    features.resize(2);
#endif
    for (size_t i = 0; i < mColorData.size(); i++) {
        const NaiveBayesClassifier &gc = mColorData[i].mClassifier;
        int feat = 0;
        features[feat++] = info->color.getHue() * 360.0 / 256.0;
        features[feat++] = info->color.getSaturation();
#ifdef USE_BRIGHTNESS_FEATURE
        features[feat++] = info->color.getBrightness();
#endif
        double dens = gc.posteriorDens(features);
        //ofLog() << "Class: " << mColorData[i].mName << " prob dens: " << dens;
        if (dens > maxFeatDens) {
            maxFeatDens = dens;
            maxFeatIndex = i;
        }
    }
    //ofLog() << "Decided it is " << mColorData[maxFeatIndex].mName;

    info->type = maxFeatIndex;
}

bool testApp::listenOsc()
{
    bool bNewMsg = false;
	// check for waiting messages
	while(mReceiver.hasWaitingMessages()){
		// get the next message
		ofxOscMessage m;
		mReceiver.getNextMessage(&m);

        if (mOnline) {
            // check for mouse moved message
            if(m.getAddress() == "/6pianos_server/step"){
                mCurStep = m.getArgAsInt32(0) + 1;
                mCurBar = m.getArgAsInt32(1);
                if(mCurStep > kNumSteps)
                    mCurStep = 0;
            }
            if(m.getAddress() == "/6pianos_server/nSteps"){
                kNumSteps = m.getArgAsFloat(0);
                mCurBar = mCurStep = 0;
            }
            

            bNewMsg = true;
        }
    }

	return bNewMsg;
}

void testApp::sendNotesOff()
{
    ostringstream ss;
    ss << "/client/" << mPlayerNum << "/chord";
    for (int i = 0; i < kNumSteps; i++) {
        ofxOscMessage msg;
        msg.setAddress(ss.str());
        msg.addStringArg("");
        msg.addIntArg(i);
        mSender.sendMessage(msg);
    }
}

void testApp::sendNotesAtStep(int inStep)
{
    // Here we expect steps in the range 0-7 (zero-based)
    if (inStep < 0 || inStep >= kNumSteps)
        ofLogWarning() << "sendNotesAtStep(): Invalid step number: " << inStep;

    const TimeStep& step = mSteps[inStep];
    for (size_t n = 0; n < step.mNotes.size(); n++) {
        ofxOscMessage msg;
        msg.setAddress("/sequencer");

        const Note& note = step.mNotes[n];

        msg.addIntArg(note.degree + (7 * note.octave));
        msg.addIntArg(note.volume);
        mSender.sendMessage(msg);
    }

}

void testApp::drawHighlight()
{
    for (size_t i = 0; i < mBlobInfo.size(); i++){

        if(mBlobInfo[i].isActive)
        {
            ofNoFill();
            ofSetColor(255, 0, 0);
            float width = mBlobInfo[i].max_pt.x - mBlobInfo[i].min_pt.x;
            float height = mBlobInfo[i].max_pt.y - mBlobInfo[i].min_pt.y;
            ofRect(mBlobInfo[i].min_pt.x, mBlobInfo[i].min_pt.y, width, height);
            ofEnableAlphaBlending();
            ofSetColor(255, 255, 255);
            ofFill();
            ofRect(mBlobInfo[i].min_pt.x, mBlobInfo[i].min_pt.y, width, height);
            ofDisableAlphaBlending();
        }
    }
}

void testApp::exit()
{
    // At exit, tell the server to turn all our notes off
    mInput.close();
    sendNotesOff();
}

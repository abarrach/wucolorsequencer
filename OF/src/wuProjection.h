
#ifndef wu_PROJECTION_H
#define wu_PROJECTION_H

#include "ofxCv.h"
#include "ofxOpenCv.h"

#define MODE_ESCACS 0
#define MODE_PUNTS 1

#define STAT_PLAY 0
#define STAT_CALIBRATION 1

class wuProjection {
public:
	wuProjection();

	void setup(int width, int height);
    void begin();
    void end();
    void calcule();
    void draw(int x=0, int y=0);
	bool movePoint(vector<ofVec2f>& points, ofVec2f point);
	void mousePressed(int x, int y, int button);
	void mouseDragged(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void keyPressed(int key);
    void drawPoints(vector<ofVec2f>& points);
    void calibraOnOff();
    void modePunts();
    void modeEscacs();
    
    ofImage left, right, warpedColor, fboImage;
    ofFbo fbo;
    
	vector<ofVec2f> leftPoints, rightPoints;
	bool movingPoint;
	ofVec2f* curPoint;
	bool saveMatrix;
	bool homographyReady;
    
    bool bCalibration;
	
	cv::Mat homography;
    
    int width, height;

    int mMode;

};

#endif

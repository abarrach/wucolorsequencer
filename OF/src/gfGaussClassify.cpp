//
//  gfGaussClassify.cpp
//  SixPianosOpenCV
//
//  Created by Glen Fraser on 12/13/12.
//
//

#include "gfGaussClassify.h"

// For mean of the hue (an "angle"), use something like this:
// x = y = 0
// foreach angle {
//     x += cos(angle)
//     y += sin(angle)
// }
// x and y /= number of samples
// average_angle = atan2(y, x)
// See: http://en.wikipedia.org/wiki/Circular_mean
// The variance would be 1 - R, where R is the length of the sample mean resultant vector,
// see: http://en.wikipedia.org/wiki/Directional_statistics
double getMean(int n, const double *pX)
{
    if (n < 1)
        return 0;
    
    double sum = 0;
    for (int i = 0; i < n; i++) {
        sum += pX[i];
    }
    return sum / n;
}

// Using "unbiased sample variance", see:
// http://en.wikipedia.org/wiki/Variance#Population_variance_and_sample_variance
double getSampleVariance(int n, const double *pX)
{
    if (n < 2)
        return 0;

    double mean = getMean(n, pX);
    double sqSum = 0;
    for (int i = 0; i < n; i++) {
        sqSum += pow(pX[i] - mean, 2);
    }
    return sqSum / (n - 1);
}

// For the covariance matrix, see: http://en.wikipedia.org/wiki/Covariance

// This is an "online" version, based on http://en.wikipedia.org/wiki/Algorithms_for_calculating_variance
// Computes both mean and variance as the data comes in.
void getMeanVariance(int n, const double *pX, double &outMean, double &outVariance)
{
    double sumSqr = 0;
    double delta = 0;

    outMean = 0;
    outVariance = 0;
    for (int i = 0; i < n; i++) {
        delta = pX[i] - outMean;
        outMean += delta / (i + 1);
        sumSqr += delta * (pX[i] - outMean);
        
    }
    outVariance = sumSqr/(n - 1);
}

void GaussTest::setupTest()
{
    double data[] = {6, 5.92, 5.58, 5.92};
    int num = sizeof(data) / sizeof(data[0]);
    double mean = getMean(num, data);
    double var = getSampleVariance(num, data);
    ofLog() << "Mean: " << mean << ", sample variance: " << var;

    data[0] = 180; data[1] = 190; data[2] = 170; data[3] = 165;
    getMeanVariance(num, data, mean, var);
    ofLog() << "Mean: " << mean << ", sample variance: " << var;
    
    data[0] = 6; data[1] = 8; data[2] = 7; data[3] = 9;
    LinearStats stats;
    for (int i = 0; i < num; i++) {
        stats.addDataPoint(data[i]);
    }
    ofLog() << "Mean: " << stats.getMean() << ", sample variance: " << stats.getVariance();
    
    NaiveBayesClassifier manClass;
    manClass.addFeature(ofPtr<GaussStatBase>(new GaussLinearStat(5.855, 3.5033e-02)));
    manClass.addFeature(ofPtr<GaussStatBase>(new GaussLinearStat(176.25, 1.2292e+02)));
    manClass.addFeature(ofPtr<GaussStatBase>(new GaussLinearStat(11.25, 9.1667e-01)));
    manClass.setProb(0.5);
    mClasses["man"] = manClass;
    
    NaiveBayesClassifier womanClass;
    womanClass.addFeature(ofPtr<GaussStatBase>(new GaussLinearStat(5.4175, 9.7225e-02)));
    womanClass.addFeature(ofPtr<GaussStatBase>(new GaussLinearStat(132.5, 5.5833e+02)));
    womanClass.addFeature(ofPtr<GaussStatBase>(new GaussLinearStat(7.5, 1.6667e+00)));
    womanClass.setProb(0.5);
    mClasses["woman"] = womanClass;
    
    CircularStats x;
    x.addDataPoint(15);
    x.addDataPoint(27);
    x.addDataPoint(39);
    x.addDataPoint(41);
    ofLog() << x.getMean() << ", " << x.getVariance();
    
}

string GaussTest::predictClass(const FeatureVec &features)
{
    NaiveBayesMapCIt it;
    double maxFeatDens = 0;
    string maxFeatLabel = "<none>";
    for (it = mClasses.begin(); it != mClasses.end(); it++) {
        const NaiveBayesClassifier &gc = it->second;
        double dens = gc.posteriorDens(features);
        ofLog() << "Class: " << it->first << " prob dens: " << dens;
        if (dens > maxFeatDens) {
            maxFeatDens = dens;
            maxFeatLabel = it->first;
        }
    }
    

    return maxFeatLabel;
}


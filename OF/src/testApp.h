
#pragma once
#include "ofMain.h"
#include "ofxOpenCv.h"
#include "wuGrayImg.h"
#include "wuImagePlus.h"
#include "ofxOsc.h"

#include "gfGaussClassify.h"
#include "wuProjection.h"


#if defined(TARGET_OSX)
#define USE_UVC_CAMERA
#ifdef USE_UVC_CAMERA
#include "ofxUVC.h"
#endif
#endif // TARGET_OSX


#define MAX_BLOBS 200

struct ColorSpot {
    string                  mName;
    int                     mDegree;
    CircularStats           mHue;
    LinearStats             mSat;
    LinearStats             mBri;
    NaiveBayesClassifier    mClassifier;
};

struct Note
{
    Note(int inDegree = 0, int inOct = 0)
    : degree(inDegree)
    , octave(inOct) {}

    int             degree;
    int             octave;
    ofPoint         pos;
    int             volume;
};

struct TimeStep
{
    vector<Note>    mNotes;
};

struct BlobInfoSt
{
    BlobInfoSt()
    : isActive(false)
    , gruix(0)
    , gr_medi(0)
    , type(0)
    , roughness(0)
    , tightness(1)
    , area(0)
    , area_per(0)
    , length(0)
    , posY(0) {}

	bool isActive;
	ofPoint min_pt;
	ofPoint max_pt;
	int gruix;
	int gr_medi;
	ofColor color;
	int type;
	int roughness; // puntiagut o suau-arrodonit
	float tightness; // (inf-1)->estret horitzontal, (1) quadrat, (1-0) estret vertical
	int area;
	float area_per;
	int length;
	float posY;
};

enum {
    STAT_NONE,
    STAT_BG
};

// ------------------------------------------------- App
class testApp : public ofBaseApp {

public:

	void setup();
	void update();
	void draw();
    void exit();


	void keyPressed  (int key);
	void mouseMoved(int x, int y );
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);

	ofColor calc_color(const ofxCvBlob * blob, unsigned char * colorPixels, unsigned char * maskPixels);
	void get_blob_info(const ofxCvBlob * blob, BlobInfoSt * info);
	void isActive_blob_info(const ofxCvBlob * blob, int inStep, BlobInfoSt * info);
    void evaluateBlobsAtStep(int inStep);
    void setWhiteBalance();

    bool loadSettings(const string &inFilename);
    void saveSettings(const string &inFilename);
    string logStats(const StatsBase &inStats) const;

    bool listenOsc();
    void sendNotesOff();
    void sendNotesAtStep(int inStep);

    void drawHighlight();

    int                     mViewportWidth, mViewportHeight;

    // CS communication
    bool                    mOnline;


	// OpenCV
	bool					mDrawBlobs;
	bool					mMaInvisible;
	bool                    mLearnBackground;
	int						mThreshold;
	int						mCaptureWidth, mCaptureHeight;
    int                     mStatus;

	// CV images
	ofVideoGrabber			mInput;
#ifdef USE_UVC_CAMERA
    ofxUVC                  mUvcControl;
    float                   mWhiteBalance;
    float                   mExposure;
#endif

	ofxCvColorImage			mColor;
	wuImagePlus				mColorPlus;
	ofxCvGrayscaleImage		mGray;
	wuGrayImg               mFons, mGrayPlus;

    ofxCvGrayscaleImage     mGrayBg;
	ofxCvGrayscaleImage 	mGrayDiff;

	// contour work
	ofxCvContourFinder		mContour;

	vector <BlobInfoSt>     mBlobInfo;

    // Histogram stuff for the first blob
    const static int        kLevelSkip = 8;
    const static int        kNumLevels = 256 / kLevelSkip;
    double                  mFracHue[kNumLevels];
    double                  mFracSat[kNumLevels];
    double                  mFracBri[kNumLevels];
    CircularStats           mStatsHue;
    LinearStats             mStatsSat;
    LinearStats             mStatsBri;


	int            kNumSteps = 8;

    vector<TimeStep>            mSteps;
    int                         mCurStep;
    int                         mCurBar;

    // Config / OSC stuff
    ofxOscSender                mSender;
    ofxOscReceiver              mReceiver;
    string                      mSenderAddr;
    int                         mSenderPort;
    int                         mReceiverPort;
    int                         mPlayerNum;
    int                         mCameraId;
    int                         mMinArea;
    int                         mMaxArea;
    int                         mHighOctaveThresh;
    int                         mLowOctaveThresh;

    bool                        mShowHistograms;
    bool                        mShowHelp;
    vector<ColorSpot>           mColorData;
    NaiveBayesClassifier        mClassifier;
    ofRectangle                 mWhiteBalanceRect;
    ofRectangle                 mResetCornersRect;
    int                         mSettingCornerId;
    double                      mWBScaleRed;
    double                      mWBScaleGreen;
    double                      mWBScaleBlue;
    ofPoint                     mWorkspaceCorners[4];

    ofTrueTypeFont              mFont;
    ofTrueTypeFont              mFontSmall;

    // HOMOGRAPHY
    wuProjection                mProjector;
    bool                        mProjectorToggle;
};
